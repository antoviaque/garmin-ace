ITEM_TYPE = {
    "t": "title",
    "w": "warning",
    "c": "caution",
    "n": "note",
    "p": "plaintext",
    "r": "challenge_response"
}

GROUP_START = "<"
GROUP_END = ">"
CHECKLIST_START = "("
CHECKLIST_END = ")"
CHALLENGE_RESPONSE_SEPARATOR = "~"
CENTERED_INDENT = "C"
SET_END = "END"
CRLF = "\r\n"

MAGIC_NUMBER_AND_REVISION = b"\xF0\xF0\xF0\xF0\x00\x01\x00\x00\x0D\x0A"

